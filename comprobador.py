#!/usr/bin/env python
# -*- coding: utf-8 -*-

suma=0
largo=0

#funcion promedio de la lista
def promedio(lista):
	largo = len(lista)
	suma = sum(lista)
	promedio = suma / largo
	return promedio

#funcion cuadrados de la lista
def cuadrados(lista):
	#cada elemento i de la lista se multiplica por si mismo
	for i in lista:
		print (i ** 2)
		
def cuenta_letras(listaB):
	#funcion max saca el string mas largo
	mas_grande = max(listaB)
	return mas_grande
	
#valores asignados de la lista a trabajar
lista = [3,7,4,3,12]
listaB = ['hola', 'chao', 'feo', 'bonito']
a = promedio(lista)
print("el promedio de la lista es:", a)
print(cuadrados(lista))
print(cuenta_letras(listaB))

